## Installation

All the dependencies are managed by npm. To install all required modules listed in the ./package.json file, run the following command: `npm install`

## Running the tests

```
mocha test.js
```