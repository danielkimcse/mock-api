const request = require('supertest')("https://reqres.in");
const chai = require('chai');

const expect = chai.expect;
const users = require('./data/users.json');
const unknown = require('./data/unknown.json');
const token = 'QpwL5tke4Pnpja7X';
const error = 'Missing password';

describe('GET /api/users', () => {

	it('returns correct components', (done) => {
		request
			.get("/api/users")
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.page).to.be.equal(users.page);
				expect(res.body.per_page).to.be.equal(users.per_page);
				expect(res.body.total).to.be.equal(users.total);
				expect(res.body.total_pages).to.be.equal(users.total_pages);
				expect(res.body.data).to.be.an.instanceof(Array);
				done();
			});
  });

	it('returns correct data', (done) => {
		request
			.get('/api/users')
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				for (let i = 0; i < users.data.length; i++) {
					expect(res.body.data[i].id).to.be.equal(users.data[i].id);
					expect(res.body.data[i].first_name).to.be.equal(users.data[i].first_name);
					expect(res.body.data[i].last_name).to.be.equal(users.data[i].last_name);
					expect(res.body.data[i].avatar).to.be.equal(users.data[i].avatar);
				}
				done();
			});
	});

	it('returns a single user', (done) => {
		request
			.get('/api/users/2')
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.data.id).to.be.equal(2);
				expect(res.body.data.first_name).to.be.equal(users.data[1].first_name);
				expect(res.body.data.last_name).to.be.equal(users.data[1].last_name);
				expect(res.body.data.avatar).to.be.equal(users.data[1].avatar);
				done();
			});
	});
});

describe('OTHER /api/users', (done) => {
	it('creates a user', (done) => {
		let user = 
		{
		    "name": "morpheus",
		    "job": "leader"
		};

		request
			.post('/api/users')
			.send(user)
			.expect(201)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.name).to.be.equal(user.name);
				expect(res.body.job).to.be.equal(user.job);
				done();
			});
	});

	it('updates a user', (done) => {
		let user = 
		{
		    "name": "morpheus",
		    "job": "zion resident"
		};

		request
			.put('/api/users')
			.send(user)
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.name).to.be.equal(user.name);
				expect(res.body.job).to.be.equal(user.job);
				done();
			});
	});

	it('deletes a user', (done) => {
		request
			.delete('/api/users/2')
			.expect(204, done);
	});
});

describe('GET /api/unknown', () => {
	it('returns correct components', (done) => {
		request
			.get("/api/unknown")
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.page).to.be.equal(unknown.page);
				expect(res.body.per_page).to.be.equal(unknown.per_page);
				expect(res.body.total).to.be.equal(unknown.total);
				expect(res.body.total_pages).to.be.equal(unknown.total_pages);
				expect(res.body.data).to.be.an.instanceof(Array);
				done();
			});		
	});

	it('returns correct data', (done) => {
		request
			.get('/api/unknown')
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				for (let i = 0; i < unknown.data.length; i++) {
					expect(res.body.data[i].id).to.be.equal(unknown.data[i].id);
					expect(res.body.data[i].name).to.be.equal(unknown.data[i].name);
					expect(res.body.data[i].year).to.be.equal(unknown.data[i].year);
					expect(res.body.data[i].pantone_value).to.be.equal(unknown.data[i].pantone_value);
				}
				done();
			});
	});

	it('returns a single resource', (done) => {
		request
			.get('/api/unknown/2')
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.data.id).to.be.equal(2);
				expect(res.body.data.name).to.be.equal(unknown.data[1].name);
				expect(res.body.data.year).to.be.equal(unknown.data[1].year);
				expect(res.body.data.pantone_value).to.be.equal(unknown.data[1].pantone_value);
				done();
			});
	});

	it('returns 404 for resource not found', (done) => {
		request
			.get('/api/unknown/23')
			.expect(404, done);
	})
});

describe('POST /api/register', (done) => {
	it('returns a token when register is a success', (done) => {
		const login = 
		{
		    "email": "sydney@fife",
		    "password": "pistol"
		};

		request
			.post('/api/register')
			.send(login)
			.expect(201)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.token).to.be.equal(token);
				done();
			});
	});

	it('returns an error message when password is missing', (done) => {
		const login =
		{
    		"email": "sydney@fife"
		}

		request
			.post('/api/register')
			.send(login)
			.expect(400)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.error).to.be.equal(error);
				done();
			});
	});
});

describe('POST /api/login', (done) => {
	it('returns a token when login is a success', (done) => {
		const login = 
		{
		    "email": "peter@klaven",
		    "password": "cityslicka"
		};

		request
			.post('/api/login')
			.send(login)
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.token).to.be.equal(token);
				done();
			});
	});

	it('returns an error message when password is missing', (done) => {
		const login =
		{
    		"email": "peter@klaven"
		}

		request
			.post('/api/login')
			.send(login)
			.expect(400)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.error).to.be.equal(error);
				done();
			});
	});
});